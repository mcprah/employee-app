export default async function ({ $auth, redirect }) {
    if (!$auth.loggedIn || $auth.strategy.token.status().expired()) {
        redirect('/login')
    }
}